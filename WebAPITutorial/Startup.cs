﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebAPITutorial.Data;
using WebAPITutorial.Middleware;
using WebAPITutorial.Services;

namespace WebAPITutorial
{
	// This class is similar to App.Module.ts in Angular. 
	// You register services here for other classes(components) to acquire from their constructor.
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			//services.AddAuthentication( options =>
			//{
			//	options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
			//	options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
			//} ).AddGoogle( options =>
			//{
			//	options.ClientId = Configuration[ "GoogleAuth:clientID" ];
			//	options.ClientSecret = Configuration[ "GoogleAuth:clientSecret" ];
			//} )
			//.AddOpenIdConnect( options =>
			//{
			//	//Configuration.Bind( "GoogleAuth", options );
			//	options.ClientId = Configuration[ "GoogleAuth:clientID" ];
			//	options.ClientSecret = Configuration[ "GoogleAuth:clientSecret" ];
			//} )
			//.AddCookie();

			// register authentication service
			services.AddAuthentication( options =>
			{
				// this converts the tokens into cookies that will be used in every subsequent request after authentication
				// add cookie authentication scheme as default
				options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;

				// force users to authenticate
				options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme; // 
			} )
			// implement the scheme services defined in add authentication
			.AddOpenIdConnect( options=>
			{
				// use 	the options that were used in registering the app to 3rd party auth service provider
				// AuthServiceProviderName needs to be defined in the appsettings.json file with clientID, authority, etc.

				Configuration.Bind( "AzureAd", options );

			} ) // for OpenIdConnect challenge scheme
			.AddCookie(); // for cookie authentication default scheme

			// services.AddScoped<IHeroData, HeroData>(); // lifetime is for each HTTP request. Typical for data access services
			//services.AddSingleton<IHeroData, HeroData>(); 
			services.AddDbContext<HeroDbContext>(
					options=>options.UseSqlServer( Configuration.GetConnectionString( "HeroDB" ) )
				);
			services.AddScoped<IHeroData, SqlHeroData>();
			services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
			IApplicationBuilder app, 
			IHostingEnvironment env)
        {
			// IApplicationBuilder instance allows us to add application middleware.
			// Every HTTP request in ASP.NET Core is processed through the middleware.
			// The order in which middleware appears matters because the requests are processed in the order they appear.
			if( env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Hero/Error");
            }
			app.UseRewriter(new RewriteOptions().AddRedirectToHttpsPermanent()); // redirect every non https requests to the HTTPS version of the same url

			app.UseStaticFiles(); // static files can be served to anyone, not only the authenticated users
			app.UseNodeModules( env.ContentRootPath ); //ContentRootPath is the absolute path of the project


			app.UseAuthentication(); // this has to happen before UseMvc because once we authenticate the user here,
			// we'll be able to use user's identity from here on out.

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Hero}/{action=Index}/{id?}" );
            });
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using WebAPITutorial.Models.Heroes;
using WebAPITutorial.Services;

namespace WebAPITutorial.ViewComponents
{
	public class HeroViewComponent : ViewComponent
    {
		private IHeroData _heroData;

		public HeroViewComponent(IHeroData heroData)
		{
			_heroData = heroData;
		}

		public IViewComponentResult Invoke()
		{
			var model = new Hero
			{
				Name = "Captain America 3",
				DOB = "1/29/1965",
				Type = HeroType.Science,
				ID = 5,
				Power = "Super Human Strength"
			};
			return View( "Default", model );
		}
    }
}

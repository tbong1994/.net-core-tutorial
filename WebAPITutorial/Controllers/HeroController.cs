﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPITutorial.Models;
using WebAPITutorial.Models.Heroes;
using WebAPITutorial.Services;
using WebAPITutorial.ViewModels;

namespace WebAPITutorial.Controllers
{
	//[Route( "[controller]/[action]" )]
	[Authorize] // make sure that the user is authenticated
	public class HeroController : Controller
	{
		private IHeroData _heroData;
		public HeroController( IHeroData heroData ) // require services from the constructor
		{
			_heroData = heroData;
		}
		[AllowAnonymous] // override authorize attribute of this controller
		public IActionResult Index()
		{
			var model = new HomeIndexViewModel();
			model.Heroes = _heroData.GetAll();
			model.CurrentMessage = "Hero Creation Successful!";
			return View( model );
		}

		//[Route( "{id}" )]
		public IActionResult About( int id )
		{
			//var hero = _heroData.GetAll().First( h => h.ID == id );
			var hero = _heroData.Get( id );
			if( hero == null )
			{
				return View( "NotFound" );
			}
			return View( hero );
		}

		[HttpGet] //Get because this is returning the view for the create
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken] // extremely important when you're authenticating users using cookies. This makes sure that the form submitted by the user is indeed the form we gave to the user, not some made-up form by malicious attackers
		public IActionResult Create( HeroEditModel model ) // prevent 'over-posting' by serializing the model to hero object and passing the hero object instead of the model.
		{
			if( ModelState.IsValid )
			{
				var newHero = new Hero
				{
					ID = model.ID,
					Name = model.Name,
					DOB = model.DOB,
					Power = model.Power,
					Type = model.Type
				};
				newHero = _heroData.Add( newHero );

				// To avoid duplicate POST requests, the response to a POST request should be a redirecto to a GET page, in this case,
				// the details page of the newly created hero.
				return RedirectToAction( nameof( About ), new
				{
					id = newHero.ID // this hero's ID is passed in to the url as id param, url/hero/about/{id}
				} );
			}
			else // invalid input model
			{
				return View();
			}


			// this is dangerous because returning a view to a POST request
			// can cause problems when the user refreshes the page. Refreshing a POST page can cause duplicate 
			// transactions, duplicate entries, etc.

			// return View( "About", newHero ); 
			
		}

		public IActionResult Error()
		{
			return View( "NotFound" );
		}
    }
}

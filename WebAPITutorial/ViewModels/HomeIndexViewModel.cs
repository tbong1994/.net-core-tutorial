﻿using System.Collections.Generic;
using WebAPITutorial.Models.Heroes;

namespace WebAPITutorial.ViewModels
{
	public class HomeIndexViewModel
    {
		public IEnumerable<Hero> Heroes
		{
			get; set;
		}
		public string CurrentMessage
		{
			get; set;
		}
	}
}

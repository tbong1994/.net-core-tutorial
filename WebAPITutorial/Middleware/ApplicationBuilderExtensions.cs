﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;

namespace WebAPITutorial.Middleware
{
	public static class ApplicationBuilderExtensions
    {
		// IApplicationBuilder's extension method to serve files from node_modules directory

		// The suggested namespace for the extension methods for IApplicationBuilder is to change the namespace
		// to Microsoft.AspNetCore.Builder, but some ppl don't like this because this extension method lives
		// inside ProjectName.Middleware, so it's up to you to decide what the namespace of this class is.
		public static IApplicationBuilder UseNodeModules( this IApplicationBuilder app, string virtualRoot )
		{
			var path = Path.Combine( virtualRoot, "node_modules" );

			var fileProvider = new PhysicalFileProvider( path );

			var options = new StaticFileOptions
			{
				RequestPath = "/node_modules", // only respond to requests that start with node_modules
				FileProvider = fileProvider // physical path of the files to look for, in response to the requests
			};

			app.UseStaticFiles( options );
			return app;
		}
    }
}

﻿using Microsoft.EntityFrameworkCore;
using WebAPITutorial.Models.Heroes;

namespace WebAPITutorial.Data
{
	public class HeroDbContext : DbContext
    {

		public HeroDbContext(DbContextOptions options) 
			: base( options )
		{

		}
		public DbSet<Hero> Heroes
		{
			get; set;
		}
	}
}

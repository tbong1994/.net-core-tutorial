﻿using System.ComponentModel.DataAnnotations;

namespace WebAPITutorial.Models.Heroes
{
	public class Hero
    {
		public int ID
		{
			get;set;
		}
		[Display(Name="Hero Name")] // this allows the Name property's display text to be "Hero Name"
		//[DataType(DataType.Passowrd)] Allows the Property to be a password-like input.
		[Required, MaxLength(50)]
		public string Name
		{
			get;set;
		}
		public string DOB
		{
			get;set;
		}
		public string Power
		{
			get;set;
		}
		public HeroType Type
		{
			get; set;
		}
	}
}

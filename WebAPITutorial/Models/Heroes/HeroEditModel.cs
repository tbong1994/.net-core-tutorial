﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPITutorial.Models.Heroes
{
    public class HeroEditModel
    {
		[Required, MaxLength(50)]
		public string Name
		{
			get; set;
		}
		[Required]
		public HeroType Type
		{
			get; set;
		}
		public string DOB
		{
			get; set;
		}
		public string Power
		{
			get; set;
		}
		public int ID
		{
			get; set;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPITutorial.Models.Heroes
{
    public enum HeroType
    {
		None,
		Strength,
		Immortality,
		Science,
		Skills
    }
}

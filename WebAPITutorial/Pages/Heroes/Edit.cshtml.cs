using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAPITutorial.Models.Heroes;
using WebAPITutorial.Services;

namespace WebAPITutorial.Pages.Heroes
{
	[Authorize]
	public class EditModel : PageModel
	{
		private IHeroData _heroData;

		[BindProperty] // bind incoming request to this property
		public Hero Hero
		{
			get; set;
		}
		public EditModel(IHeroData heroData)
		{
			_heroData = heroData;
		}
		public IActionResult OnGet(int id)
		{
			Hero = _heroData.Get( id );
			if( Hero == null )
			{
				return RedirectToAction( "Index", "Hero" );
			}
			return Page(); // this is similar to View(). The page to be returned is THIS page, in this case, Edit.cshtml.
		}
		
		public IActionResult OnPost() // name of the HTTP Handlers matteres. OnGet, OnPost, OnPut, etc.
		{
			if( !ModelState.IsValid )
			{
				return RedirectToAction( "Index", "Hero" );
			}
			_heroData.Update( Hero );
			return RedirectToAction( "About", "Hero", new
			{
				id = Hero.ID
			} );
			
		}
	}
}
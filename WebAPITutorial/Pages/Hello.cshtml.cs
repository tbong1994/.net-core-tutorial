using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAPITutorial.Models.Heroes;
using WebAPITutorial.Services;

namespace WebAPITutorial.Pages
{
	public class HelloModel : PageModel
    {
		private IHeroData _heroData;
		public string Message
		{
			get; set;
		}
		public HelloModel(IHeroData data)
		{
			_heroData = data;
		}
        public void OnGet(int id, string name)
        {
			Message = $"Hello {name} with id {id}";
        }
    }
}
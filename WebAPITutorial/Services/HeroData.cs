﻿using System.Collections.Generic;
using System.Linq;
using WebAPITutorial.Models.Heroes;

namespace WebAPITutorial.Services
{
	public class HeroData /*: IHeroData*/
	{
		List<Hero> _heroes;

		public HeroData()
		{
			_heroes = new List<Hero>
			{
				new Hero{ Name="Iron Man", DOB="5/17/1978", ID=1, Power="Super Intelligence"},
				new Hero{ Name="Thor", DOB="100BC", ID=2, Power="God of Thunder"},
				new Hero{ Name="Captain America", DOB="1/28/1914", ID=0, Power="Super Strength"}
			};
		}

		public IEnumerable<Hero> GetAll()
		{
			return _heroes.OrderBy( hero => hero.ID );
		}
		public Hero Get( int id )
		{
			return _heroes.Find( hero => hero.ID == id );
		}

		public Hero Add( Hero newHero )
		{
			newHero.ID = _heroes.Max( h => h.ID ) + 1;
			_heroes.Add( newHero );
			return newHero;
		}
	}
}

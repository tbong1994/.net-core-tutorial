﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPITutorial.Models.Heroes;

namespace WebAPITutorial.Services
{
    public interface IHeroData
    {
		IEnumerable<Hero> GetAll();
		Hero Get( int id );
		Hero Add( Hero newHero );
		Hero Update( Hero updatedHero );
    }
}

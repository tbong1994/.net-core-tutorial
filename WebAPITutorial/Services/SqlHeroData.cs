﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebAPITutorial.Data;
using WebAPITutorial.Models.Heroes;
using Microsoft.EntityFrameworkCore;

namespace WebAPITutorial.Services
{
	// this class is a entity framework class to work as a middle man between IHeroData interface and the actual databse.
	public class SqlHeroData : IHeroData
	{
		private HeroDbContext _context;

		public SqlHeroData(HeroDbContext context)
		{
			_context = context;
		}
		public Hero Add( Hero newHero )
		{
			_context.Heroes.Add( newHero );
			
			// this is the actual insertion execution. without savechanges(), new data will not be saved. Which means you can delay this operation to a later time depending on what you need to do.
			_context.SaveChanges();

			// the reason why we're not manually generating the ID for the new hero is because the SQLServer automatically
			// generates the id column and assigns that id to the new hero.

			return newHero;
		}

		public Hero Get( int id )
		{
			return _context.Heroes.FirstOrDefault( h => h.ID == id );
		}

		public IEnumerable<Hero> GetAll() // IQueryable should be used if the db was a large set of data
		{
			return _context.Heroes.OrderBy( h => h.ID );
		}

		public Hero Update( Hero updatedHero )
		{
			// we're updating, not creating new hero
			_context.Attach( updatedHero ).State = EntityState.Modified;
			_context.SaveChanges();

			return updatedHero;
		}
	}
}
